<?php

class View
{
    public function __construct($pubAddr2)
    {
        $this->pubAddr2 = $pubAddr2;
    }

    // Address Checksum
    public $pubAddrChksum;

    // Checksum
    public $pubAddrChksum2;

    // Netbyte + 10
    public $pubAddrForHash;

    // Hash of 12
    public $pubAddrHash;

    // Public Address Hex
    public $pubAddrHex;

    // Public Address
    public $pubAddr2;

    // Public Spend Key
    public $pubSpend2;

    // Public View Key
    public $pubView2;

    // Error Message
    public $validNo;

    // Success Message
    public $validYes;

    // System Message
    public $message;
}
