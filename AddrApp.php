<?php

class AddrApp
{
    const COINS = [
        '12' 	=> 'Standard XMR',
        '01' 	=> 'BBR',
        '48' 	=> '(the real) DaSH',
        'b201' 	=> 'AEON',
        '35' 	=> 'XMR Testnet',
        '13' 	=> 'XMR Integrated',
        '11' 	=> 'XMR Truncated',
    ];

    public function check(View $view)
    {
        $view = $this->clear($view);
        $addr58 = $view->pubAddr2;

        $addrHex = Base58::decode($addr58);

        if (strlen($addr58) !== 95 &&
            strlen($addr58) !== 97 &&
            strlen($addr58) !== 51 &&
            strlen($addr58) !== 106
        ) {
            $view->validNo = "Invalid Address Length: " . strlen($addr58);

            return $view;
        }

        if (strlen($addrHex) === 140){
            $netbyte = slice($addrHex, 0, 4);
        } else {
            $netbyte = slice($addrHex, 0, 2);
        }

        if ($netbyte === '13'){
            if (strlen($addrHex) !== 154){
                $view->validNo = 'Invalid Address Length: ' . strlen($addr58) . ' for ' . self::COINS[$netbyte];

                return $view;
            }
        }

        if ($netbyte === '11'){
            if (strlen($addrHex) !== 74){
                $view = $this->clear($view);
                $view->validNo = 'Invalid Address Length: ' . strlen($addr58) . ' for ' . self::COINS[$netbyte];

                return $view;
            }

        } else if (strlen($addrHex) === 140){
            $view->pubView2 = slice($addrHex, 68,132);
        } else {
            $view->pubView2 = slice($addrHex, 66, 130);
        }

        if (($netbyte !== '11' && $netbyte !== '13') && strlen($addrHex) !== 138 && strlen($addrHex) !== 140){
            $view = $this->clear($view);
            $view->validNo = 'Invalid Address Length: ' . strlen($addr58) . ' for ' . self::COINS[$netbyte];

            return $view;
        }

        $addrHash = $this->cnFastHash(slice($addrHex, 0,-8));

        $view->pubAddrHex = $addrHex;

        if (strlen($addrHex) === 140){
            $view->pubSpend2 = slice($addrHex, 4, 68);
        } else {
            $view->pubSpend2 = slice($addrHex, 2, 66);
        }

        $view->pubAddrChksum = slice($addrHex, -8);
        $view->pubAddrForHash = slice($addrHex, 0, -8);
        $view->pubAddrHash = $addrHash;
        $view->pubAddrChksum2 = slice($addrHash, 0, 8);

        if (slice($addrHex, -8) == slice($addrHash, 0, 8)) {
            $view->validYes = 'Yes! This is a valid ' . self::COINS[$netbyte] . ' address.';
        } else {
            $view->validNo = 'No! Checksum invalid!';
        }

        return $view;
    }

    public function show(View $view)
    {
        echo
            'Address Checksum: ' . $view->pubAddrChksum . "\n\r" .
            'Checksum: ' . $view->pubAddrChksum2 . "\n\r" .
            'Netbyte + 10: ' . $view->pubAddrForHash . "\n\r" .
            'Hash of 12: ' . $view->pubAddrHash . "\n\r" .
            'Public Address Hex: ' . $view->pubAddrHex . "\n\r" .
            'Public Address: ' . $view->pubAddr2 . "\n\r" .
            'Public Spend Key: ' . $view->pubSpend2 . "\n\r" .
            'Public View Key: ' . $view->pubView2 . "\n\r" .
            'System Message: ' . $view->message . "\n\r" .
            'Error Message: ' . $view->validNo . "\n\r" .
            'Success Message: ' . $view->validYes . "\n\r"
        ;
    }

    private function clear(View $view)
    {
        $view->pubAddrHex = null;
        $view->pubSpend2 = null;
        $view->pubView2 = null;
        $view->pubAddrChksum = null;
        $view->pubAddrForHash = null;
        $view->pubAddrHash = null;
        $view->pubAddrChksum2 = null;
        $view->validYes = null;
        $view->validNo = null;
        $view->message = null;

        return $view;
    }

    private function cnFastHash($input)
    {
        if (strlen($input) % 2 !== 0 || !ctype_xdigit($input)) {
            throw new Exception('Input invalid');
        }

        return (new Keccak)->hash(hexToBin($input));
    }
}
