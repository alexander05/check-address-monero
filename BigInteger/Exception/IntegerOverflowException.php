<?php

//declare(strict_types=1);

namespace Brick\Math\Exception;

use Brick\Math\BigInteger;

/**
 * Exception thrown when an integer overflow occurs.
 */
class IntegerOverflowException extends MathException
{
    /**
     * @param BigInteger $value
     *
     * @return IntegerOverflowException
     */
    public static function toIntOverflow(BigInteger $value)
    {
        $message = '%s is out of range %d to %d and cannot be represented as an integer.';

        return new self(sprintf($message, (string) $value, PHP_INTMIN, PHP_INTMAX));
    }
}
