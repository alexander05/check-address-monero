<?php

use Brick\Math\BigInteger;
use Brick\Math\RoundingMode;

function to32Bits($value)
{
    $value = ($value & 0xffffffff);
    if ($value & 0x80000000) $value = -((~$value & 0xffffffff) + 1);
    return $value;
}

function logicRightShift($a, $b)
{
    if ($b >= 32 || $b < -32) {
        $m = (int)($b/32);
        $b = $b-($m*32);
    }

    if ($b < 0) {
        $b = 32 + $b;
    }

    if ($b == 0) {
        return (($a>>1)&0x7fffffff)*2+(($a>>$b)&1);
    }

    if ($a < 0)
    {
        $a = ($a >> 1);
        $a &= 2147483647;
        $a |= 0x40000000;
        $a = ($a >> ($b - 1));
    } else {
        $a = ($a >> $b);
    }

    return $a;
}

function slice($value, $start, $stop = null)
{
    return implode('', array_slice(str_split($value), $start, $stop));
}

function Uint8Array($length)
{
    $res = [];
    foreach(range(1, $length) as $item) {
        $res[] = 0;
    }

    return $res;
}

function uint64To8be($num, $size)
{
    $res = Uint8Array($size);
    if ($size < 1 || $size > 8) {
        throw new Exception("Invalid input length");
    }
    $twopow8 = BigInteger::of(2)->power(8);
    for ($i = $size - 1; $i >= 0; $i--) {
        $res[$i] = BigInteger::of($num)->remainder($twopow8)->toInt(); // .toJSValue()
        $num = BigInteger::of($num)->dividedBy($twopow8, RoundingMode::DOWN);
    }

    return $res;
}

function set($array, $insert, $index)
{
    $length = count($insert);

    for ($i = $index; $i < $length + $index; $i++) {
        $array[$i] = array_shift($insert);
    }
    return $array;
}

function binToHex($bin)
{
    $out = [];
    for ($i = 0; $i < count($bin); ++$i) {
        $out[] = slice("0" . (string) base_convert($bin[$i], 10, 16), -2);
    }

    return implode('', $out);
}

function strToBin($str)
{
    $res = Uint8Array(strlen($str));

    for ($i = 0; $i < strlen($str); $i++) {
        $res[$i] = ord($str[$i]);
    }

    return $res;
}

function hexToBin($hex)
{
    if (strlen($hex) % 2 !== 0) throw new Exception('Hex string has invalid length!');

    $res = [];
    foreach(range(1, (strlen($hex) / 2)) as $item) {
        $res[] = 0;
    }

    for ($i = 0; $i < strlen($hex) / 2; ++$i) {
        $res[$i] = intval(
            slice(
                $hex,
                $i * 2,
                ($i * 2 + 2) - $i * 2
            ), 16
        );
    }

    return $res;
}

function getPubAddr2()
{
    if (array_key_exists(1, $_SERVER['argv'])){
        return $_SERVER['argv'][1];
    }

    return null;
}