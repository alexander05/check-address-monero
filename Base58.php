<?php

use Brick\Math\BigInteger;

class Base58
{
    const FULL_BLOCK_SIZE = 8;
    const FULL_ENCODED_BLOCK_SIZE = 11;
    const ENCODED_BLOCK_SIZES = [0, 2, 3, 5, 6, 7, 9, 10, 11];
    const ALPHABET_STR = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz";

    public static function decode_block($data, $buf, $index)
    {
        if (count($data) < 1 || count($data) > self::FULL_ENCODED_BLOCK_SIZE) {
            throw new Exception("Invalid block length: " . count($data));
        }

        $res_size = array_search(count($data), self::ENCODED_BLOCK_SIZES);

        if ($res_size <= 0) {
            throw new Exception("Invalid block size");
        }

        $res_num = BigInteger::of(0);
        $order = BigInteger::of(1);

        $alphabet = [];
        for ($i = 0; $i < strlen(self::ALPHABET_STR); $i++) {
            $alphabet[] = ord(self::ALPHABET_STR[$i]);
        }
        $alphabet_size = count($alphabet);

        for ($i = count($data) - 1; $i >= 0; $i--) {
            $digit = array_search($data[$i], $alphabet);

            if ($digit < 0) {
                throw new Exception("Invalid symbol");
            }

            $product = (string) BigInteger::of($order)
                ->multipliedBy($digit)
                ->plus($res_num)
            ;

            if (BigInteger::of($product)->compareTo(BigInteger::of(2)->power(64)) === 1) {
                throw new Exception("Overflow");
            }

            $res_num = $product;
            $order = (string) (BigInteger::of($order)->multipliedBy($alphabet_size));
        }

        if ($res_size < self::FULL_BLOCK_SIZE && (BigInteger::of(2)->power(8 * $res_size)->compareTo($res_num) <= 0)) {
            throw new Exception("Overflow 2");
        }

        $buf = set($buf, uint64To8be($res_num, $res_size), $index);

        return $buf;
    }

    public static function decode($enc)
    {
        $enc = strToBin($enc);
        if (count($enc) === 0) {
            return '';
        }

        $full_block_count = floor(count($enc) / self::FULL_ENCODED_BLOCK_SIZE);
        $last_block_size = count($enc) % self::FULL_ENCODED_BLOCK_SIZE;
        $last_block_decoded_size = array_search($last_block_size, self::ENCODED_BLOCK_SIZES);
        if ($last_block_decoded_size === null) {
            throw new Exception("Invalid encoded length");
        }

        $data_size = $full_block_count * self::FULL_BLOCK_SIZE + $last_block_decoded_size;
        $data = Uint8Array($data_size);

        for ($i = 0; $i < $full_block_count; $i++) {
            $data = self::decode_block(
                array_slice(
                    $enc,
                    $i * self::FULL_ENCODED_BLOCK_SIZE,
                    ($i * self::FULL_ENCODED_BLOCK_SIZE + self::FULL_ENCODED_BLOCK_SIZE) - ($i * self::FULL_ENCODED_BLOCK_SIZE)
                ),
                $data,
                $i * self::FULL_BLOCK_SIZE
            );
        }

        if ($last_block_size > 0) {
            $data = self::decode_block(
                array_slice(
                    $enc,
                    $full_block_count * self::FULL_ENCODED_BLOCK_SIZE,
                    ($full_block_count * self::FULL_ENCODED_BLOCK_SIZE + $last_block_size) - ($full_block_count * self::FULL_ENCODED_BLOCK_SIZE)
                ),
                $data,
                $full_block_count * self::FULL_BLOCK_SIZE
            );
        }

        return binToHex($data);
    }
}

