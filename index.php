<?php

define('DEFAULT_PUB_ADDR2', '42k1E1mybx3ixAMwPzVviWXHQ4y9vsYwzBRbMJ1hYSL3cWV7paD6oyCGbj3wiw69PdCojxAxsb8M6KQgTK6FUR4Z5DABMkm');

require_once 'load.php';

/**
 * Примеры валидных pubAddr2 -
 * 4BAiAmZo6tLA4EUn2ZXJu5XuYbZedcpURRnjQBCheyTYR2roL814xuJW6qW2wCcQcJimn13QUz5JpTEJX8JDdyLUViP8Kb
 * 42fo9aimczDVQYBxEanrGEA5ZqFfeHEVZGdRDCJuk9AoLHc31oxoCEEeTE6q3uTYRySN4fjkCdCo643kAVS661RKAwMGVki
 * 43povqLJEfv592BNEGDGTxY95ukphMz4iM59ogJcQx8jUixo2xUDMXYSzp3kJCb4VkX4HHrU8B7PPMTp2Mv9L2fpTkTaTdU
 * 42k1E1mybx3ixAMwPzVviWXHQ4y9vsYwzBRbMJ1hYSL3cWV7paD6oyCGbj3wiw69PdCojxAxsb8M6KQgTK6FUR4Z5DABMkm
 *
 * Получены на странице https://xmr.llcoins.net/addresstests.html
 * Нажатие на кнопку Random MyMonero
 * Результат генерации в поле Public Address
 *
 */
$pubAddr2 = getPubAddr2();

/**
 *
 * Если не передан параметр, подставляется дефолтный
 *
 * Пример вызова с переданным параметром адреса monero:
 * php index.php 42fo9aimczDVQYBxEanrGEA5ZqFfeHEVZGdRDCJuk9AoLHc31oxoCEEeTE6q3uTYRySN4fjkCdCo643kAVS661RKAwMGVki
 *
 * Пример вызова без переданного параметра адреса monero:
 * php index.php
 *
 */
$view = new View($pubAddr2 ?: DEFAULT_PUB_ADDR2);

try {
    $app = new AddrApp();
    $app->check($view);

} catch (Exception $e) {
    $view->validNo = 'No! Address invalid!';
    $view->message = $e->getMessage();
}

$app->show($view);

